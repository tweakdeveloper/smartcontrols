@interface SBAwayView : NSObject {
}

- (void)showMediaControls;

@end

@interface SBAwayController : NSObject {
}

+ (SBAwayController *)sharedAwayController;
- (SBAwayView *)awayView;

@end

@interface SBMediaController : NSObject {
}

+ (SBMediaController *)sharedInstance;
- (BOOL)isPlaying;

@end

%hook SBAwayController

- (void)_undimScreen {
  SBAwayController *awayController = [%c(SBAwayController)
    sharedAwayController];
  SBAwayView *awayView = [awayController awayView];
  SBMediaController *mediaController = [%c(SBMediaController) sharedInstance];
  if([mediaController isPlaying]) {
    [awayView showMediaControls];
  }
  %orig;
}

%end
