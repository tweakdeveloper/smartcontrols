# SmartControls #

This is a package for Whited00r in order to get the automatic media controls when music is playing.  To install it, simply
```
#!bash
make package install
```

## Note ##
You may need to remove the theos symlink and recreate it yourself:
```
#!bash
rm theos
ln -s $THEOS .
```
